let fileEnv;
try {
    fileEnv = require('../env.json');
} catch(err) {
    console.error(`Couldn't load env.json: ${err.message}`);
}

let env = new Proxy(process.env, {
    get(target, prop) {
        if (fileEnv && prop in fileEnv) {
            return fileEnv[prop];
        }

        return target[prop];
    }
});

export default env;