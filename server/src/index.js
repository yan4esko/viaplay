import app from './app';
import env from './util/env';

let port = env.PORT || 3000;
app.listen(port, function() {
    console.log(`Listening on port ${port}...`);
});