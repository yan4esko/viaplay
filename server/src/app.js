import express from 'express';
import cors from 'cors';
import controllers from './controllers';

let app = express();

app.use(cors());
app.use(controllers);

export default app;