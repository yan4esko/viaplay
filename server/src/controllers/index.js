import movies from './movies';

import {Router} from 'express';

let router = new Router();
router.use('/movies', movies);

export default router;