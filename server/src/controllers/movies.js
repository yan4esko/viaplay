import {
    getMovieInfo,
    getTopRatedMovies
} from '../services/movies';

import {Router} from 'express';

let router = new Router();
router.get('/', async function(req, res, next) {
    try {
        let movies = await getTopRatedMovies({page: req.query.page, language: req.query.language});
        res.send(movies);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', async function(req, res, next) {
    try {
        let movieInfo = await getMovieInfo({id: req.params.id});
        res.send(movieInfo);
    } catch (err) {
        next(err);
    }
});

export default router;