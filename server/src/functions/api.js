import serverlessHttp from 'serverless-http';
import express from 'express';
import app from '../app';

let netlifyApp = express();
netlifyApp.use('/.netlify/functions/api', app);

exports.handler = serverlessHttp(netlifyApp);