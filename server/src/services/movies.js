import movieDb from 'moviedb-promise';
import env from '../util/env';
import _ from 'lodash';

let _db = new movieDb(env.TMDB_API_KEY);

let _setImagePaths = function(movie) {
    movie.backdrop_path = `https://image.tmdb.org/t/p/w780${movie.backdrop_path}`;
    movie.poster_path = `https://image.tmdb.org/t/p/w342${movie.poster_path}`;
};

let _prepMovieShort = function(movie) {
    movie = _.pick(movie, 
        'id', 
        'imdb_id',
        'vote_average', 
        'title', 
        'tagline',
        'original_title', 
        'poster_path', 
        'backdrop_path'
    );

    _setImagePaths(movie);

    return movie;
};

let _prepMovieFull = function(movie) {
    _setImagePaths(movie);

    movie.genres = _.map(movie.genres, 'name');
    movie.production_countries = _.map(movie.production_countries, 'name');

    let trailer = _.find(movie.videos.results, {
        site: 'YouTube'
    });

    if (trailer) {
        movie.trailer_yt_key = trailer.key;
    }

    movie.actors = _.map(_.take(movie.credits.cast, 5), 'name');
    movie.director = _.head(movie.credits.crew).name; // assuming first is director

    movie = _.pick(movie, 
        'id', 
        'title', 
        'original_title', 
        'poster_path', 
        'backdrop_path',
        'genres',
        'overview',
        'production_countries',
        'release_date',
        'runtime',
        'vote_average', 
        'vote_count',
        'trailer_yt_key',
        'actors',
        'director'
    );

    return movie;
};

export let getTopRatedMovies = async function({page = 1, language = 'en-US'} = {}) {
    let response = await _db.miscTopRatedMovies({page, language});
    response.results = _.map(response.results, _prepMovieShort);

    return response;
};

export let getMovieInfo = async function({id}) {
    let movie = await _db.movieInfo({id}, {append_to_response: 'videos,credits'});
    return _prepMovieFull(movie);
};

export default exports;