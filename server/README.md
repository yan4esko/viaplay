# Viaplay server

NOTE: for a local run (`npm run serve`), you need to create a file `./src/env.json` like 
```
{
  "TMDB_API_KEY": "<your TMDB API key>"
}
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles for Netlify Functions
```
npm run dist
```

### Lints and fixes files
```
npm run lint
```