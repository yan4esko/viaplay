let fs = require('fs');
let _ = require('lodash');

let env = {
    TMDB_API_KEY: process.env.TMDB_API_KEY
};

let fail = false;
_.forIn(env, function(val, key) {
    if (!val) {
        console.error(`Env variable ${key} not set!`);
        fail = true;
    }
});

if (fail) {
    process.exit(1);
}

fs.writeFileSync(__dirname + '/../src/env.json', JSON.stringify(env, null, 2));