let webpack = require('webpack');

module.exports = {
    target: 'node',
    mode: 'development',
    devtool: 'source-map',

    entry: {
        'functions/api': './src/functions/api.js'
    },

    output: {
        filename: '[name].js',
        path: __dirname + '/dist',
        libraryTarget: 'commonjs'
    },

    plugins: [
        new webpack.DefinePlugin({ 'global.GENTLY': false })
    ],
};