import axios from 'axios';

let API_BASE_URL = 'http://localhost:3000';

if (window.document.location.hostname !== 'localhost') {
  API_BASE_URL = '/.netlify/functions/api';
}

export let getMovies = async function() {
  let movies = await axios.get(API_BASE_URL + '/movies');
  return movies.data.results;
};

export let getMovie = async function({ id }) {
  let movie = await axios.get(API_BASE_URL + '/movies/' + id);
  return movie.data;
};

export default {
  getMovie,
  getMovies
};
