import Vue from 'vue';
import Router from 'vue-router';
import Movies from './views/Movies.vue';
import MovieInfo from './views/MovieInfo.vue';
import Stub from './views/Stub.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/film' // only for the PoC
    },
    {
      path: '/film/:id',
      component: MovieInfo
    },
    {
      path: '/film',
      name: 'movies',
      component: Movies,
      meta: {
        title: 'Film'
      }
    },
    {
      path: '/serier',
      name: 'series',
      component: Stub
    },
    {
      path: '/sport',
      name: 'sport',
      component: Stub
    },
    {
      path: '/barn',
      name: 'children',
      component: Stub
    },
    {
      path: '/hyrbutik',
      name: 'rental',
      component: Stub
    }
  ]
});
