# Viaplay 

Hey there! 

## Deployed version

The `master` branch is automatically deployed and can be found at https://viaplay.iansavchenko.com/. 

## Running locally 

If you want to run it locally, run `npm run serve` in both `./client` and `./server` directories. Please, be aware that you need to provide an API key for TMDB for the server (check [server's readme](./server/README.md)).

## Client notes

The client is a VueJS SPA, scaffolded with Vue-CLI. 

What was left out of scope:
* Reponsive design. In the end I decided to have a fixed width content, 
it takes a lot of time to make everything properly responsive. 

* Interactive elements like "user menu" and "search box" not implemented.

* Most of the links in the app don't work or endup with stub, obviously.

* Caching not implemented.

* Tests - I'm a huge proponent of tests, but didn't have inspiration and energy for them this time with a one-off project (I had them for other stuff!). 

What I've done extra:

* Movies list page - thought it will be more fun to browse a list of movies 
and then pick one.

* Loading animation and outline - used only in one place, but could do the same 
in the MovieInfo view too (really like this effect in FB in LinkedIn).

* Only YouTube trailers are supported and some of the movies don't have one, 
so there is no play button.

* Tons of other pixel pushings (very rewarding).

* Deployment with Netlify - doesn't really require anything, really, 
when Vue-CLI is setup and produces dist, but still fun to see it live.

* Client checks current host and in the case it's not `localhost`, 
tries to use Netlify Functions.

## Server notes

The server is an Express app, written from scratch mostly (no scaffolding).

I didn't use Viaplay's API, as was suggested in the task, but decided to 
go for TMDB completely.

Server does requests to TMDB API and massages the data into a form, 
suitable for the client. Some extra possibilities were exposed 
(choise of language and pagination), but they were not used.

Local serving with `npm run serve` uses nodemon and Babel CLI (primarily, because I prefer `import` syntax over `require`).

Server is deployed with Netlify Functions. This requires each function to be 
a single file, so I set up Webpack and `npm run dist` target 
(there were some difficulties on the way). 
The function itself runs the same Expess app, wrapped with `serverless-http` 
utility. 

Secrets management is implemented in a simple, but working way: 
there must be `src\env.json` file with env variables. 
So variables are read either from there or from the environment. 
File is more handy when running locally and env variables are not supported 
in the runtime with Netlify Functions (though are they supported during build/deploy). So in the latter case, the file is generated during the build with `scripts/create-env-json.js`. 

Deploy settings and scripts are set in `netlify.toml`.